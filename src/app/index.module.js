(function () {
  'use strict';

  angular
    .module('test-task', [
      'ngAnimate',
      'ngCookies',
      'ngTouch',
      'ngSanitize',
      'ngMessages',
      'ngAria',
      'ngResource',
      'ui.router',
      'toastr',
      'ui.mask'
    ]);

})();
