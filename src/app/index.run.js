(function () {
  'use strict';

  angular
    .module('test-task')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
