/* global describe, beforeEach, it */
(function () {
  'use strict';

  describe('directive address', function () {
    var $scope, element, form;

    beforeEach(module('test-task'));
    beforeEach(inject(function ($compile, $rootScope) {
      $scope = $rootScope.$new();
      element = angular.element(
        '<form name="form">' +
        '<input ng-model="model.address" name="address" address>' +
        '</form>'
      );

      $scope.model = { phoneNumber: null };
      $compile(element)($scope);
      form = $scope.form;
    }));

    it('should be compiled', function () {
      expect(element.html()).not.toEqual(null);
    });

    it('should pass with valid address', function () {
      var testString = 'Test address';
      form.address.$setViewValue(testString);
      $scope.$digest();
      expect($scope.model.address).toEqual(testString);
      expect(form.address.$valid).toBeTruthy();
    });

    it('should not pass with wrong address', function () {
      var testString = 'wrong_test_address';
      form.address.$setViewValue(testString);
      $scope.$digest();
      expect($scope.model.address).toBeUndefined();
      expect(form.address.$valid).toBeFalsy();
    });
  });
})();
