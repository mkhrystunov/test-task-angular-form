(function () {
  'use strict';

  angular
    .module('test-task')
    .directive('address', address);

  var ADDRESS_REGEX = /^wrong_test_address$/i;

  // TODO should really test address for availability
  /** @ngInject */
  function address() {
    return {
      require: '?ngModel',
      restrict: 'A',
      link: function (scope, element, attributes, controller) {
        controller.$validators.address = function (modelValue, viewValue) {
          return !ADDRESS_REGEX.test(viewValue);
        };
      }
    };
  }

})();
