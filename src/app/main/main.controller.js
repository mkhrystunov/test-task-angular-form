(function () {
  'use strict';

  angular
    .module('test-task')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController(toastr) {
    var vm = this;

    vm.resetForm = function (form) {
      vm.contact = null;
      form.$setPristine();
      form.$setUntouched();
      toastr.info('Сообщение отправлено!');
    };

    return vm;
  }
})();
