/* global describe, beforeEach, it, jasmine */
(function () {
  'use strict';

  describe('controllers', function () {
    var vm;

    beforeEach(module('test-task'));
    beforeEach(inject(function (_$controller_) {
      vm = _$controller_('MainController');
    }));

    it('should reset form', function () {
      vm.contact = 'not null';
      var form = jasmine.createSpyObj('form', ['$setPristine', '$setUntouched']);
      vm.resetForm(form);
      expect(vm.contact).toBe(null);
      expect(form.$setPristine).toHaveBeenCalled();
      expect(form.$setUntouched).toHaveBeenCalled();
    });
  });
})();
